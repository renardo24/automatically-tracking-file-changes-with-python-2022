# ABOUT

In this liveProject, you will fill the role of a software developer at PharmaDeal Iberia, which is a pharmaceutical supply chain company that operates across southern Spain and Portugal. You have been given the task to learn and work with the company’s audit team, to implement a Python tool that can produce a report of what important documents change through the organization and their frequency. This tool will be deployed and used per user. Your work as a developer in understanding how to find what files change across an organization is crucial, as compliance regulations across the EU become tighter by the day, there is a need within PharmaDeal to track what important documents change within the organization so that the company’s document audit team ensures that all-important documents comply with all industry and EU regulations—which is something that Microsoft Office doesn’t have out-of-the-box support for.

Therefore, your assignment at PharmaDeal is as follows:

- Create and connect to a local SQLite database
- Create tables and cursors specifically designed for tracking file changes
- Understand and work with file information and MD5 hashes
- Understand and work with tables specifically designed for tracking file changes
- Apply Automation principles to the tool
- Generate a Change Report

## Techniques employed

The following are some of the techniques you’ll employ throughout this project. Don’t worry if you haven’t mastered any of these areas—we’ll give you the necessary resources to learn more about each.

Automation developers use a diverse range of techniques to achieve the goal set forth for this project, many of which are picked up on the job.

Listed under the bullet points that follow, are the steps required to implement this project.

* Create and connect to a local SQLite database
  * Usage of the built-in Python OS, sys, and SQLite libraries
  * Write a series of Python functions that can create an SQLite instance
  * Write a Python function that can query the master database for the SQLite instance created
  * Write a series of Python functions that can create and connect to a database hosted within the SQLite instance

* Create tables and cursors specifically designed for tracking file changes
  * Write a series of Python functions that can create file-level tracking database tables on an SQLite instance
  * Write a Python function that can create file-level tracking table indexes on an SQLite instance
  * Write a Python function that can perform query the master database and determine if a database table exists on an SQLite instance
  * Write a Python function that can create a database table cursor on an existing database table, on an SQLite instance

* Understand and work with file information and MD5 hashes
  * Write a Python function that can read file-level access and DateTime info on any given file
  * Write a Python function that can generate an MD5 hash for any given file
  * Write a Python function that can determine (calculate) the MD5 hash of any given file, which will be later used to determine if a file has changed
  * Write a Python function that can secure a file using an MD5 hash

* Understand and work with tables specifically designed for tracking file changes
  * Write a Python function that can insert file-level data into database tables using the SQLite INSERT command
  * Write a Python function that can update file-level data into database tables using the SQLite UPDATE command
  * Write a Python function that can remove file-level data from database tables using the SQLite DELETE command
  * Write a Python function that can query file-level data from database tables using the SQLite SELECT command

* Apply Automation principles to the tool
  * Write a Python function that can traverse (navigate) through a complete folder structure (including subfolders) and can filter out specific file types
  * Write a Python function that can monitor file changes on specific files types and folders
  * Write a Python function that can query file data on an SQLite instance after detecting file changes, using calculated MD5 hashes to check for file differences

* Generate a Change Report
  * Create a series of Python functions that can use the openpyxl library to create an Excel report, using built-in functions provided by the openpyxl library
  * Create a Python function that can aggregate existing data gathered through file monitoring and consolidate it into an Excel report, using built-in functions provided by the openpyxl library
  * Create a batch file that can invoke Python and run the script, which can be executed through the command line, during operating system startup, or through a scheduled task. This technique will allow the finalized tool to easily be deployed and executed on a Windows environment.

Note: The company-wide consolidation of the various Excel reports from each user machine, is beyond the scope of the project. The tool that you will be creating throughout this project produces a report of the files that change, per user/machine only.

## Project outline

The project consists of the following parts along with an executive summary.

1. Creating and connecting to a local SQLite database
2. Creating tables and cursors specifically designed for tracking file changes
3. Working with file information and MD5 hashes
4. Implementing file tracking main functions
5. Applying automation principles to the tool
6. Generating a change report

## Executive Summary

The skills covered in order are the following: working with queries, cursors, and tables with SQLite, retrieving file information and generating MD5 hashes, monitoring files for changes, and using openpyxl to generate a report from data stored in SQLite tables.

The deliverable for each milestone is a Python script (uploaded preferably to GitHub) documenting your workflow and results.

The final executive summary will be a notebook or Word document converted to HTML or PDF to share the conclusions with your manager at PharmaDeal.

Each section builds upon the previous and will test your skills in a different area of file-tracking automation with Python. As you progress throughout the project, keep in mind the overall objective: how to track files change across an organization, individually per user (per machine).

The project is a good example and representation of some of the problems developers face within their organizations to automate repetitive business processes. It utilizes some of the most popular libraries available in Python to achieve the goal set forth.

## Environment

To be able to make the most out of this project, it is highly recommended to use Visual Studio Code as a coding environment and editor. The version and type of operating system being used is irrelevant, although the author has used Windows 10 during the development and testing phases.

## Python libraries and setup

This course uses Python 3.6, but it’s also possible to use Python 3.7 or 3.8. It is recommended to use the official Python distributions found on the official Python website.

The following libraries will be utilized in this liveProject—it is not required to have working experience with them, as you’ll be able to pick up the basics by reading the documentation and putting them to use.

os: Miscellaneous interfaces, using operating system dependent functionality
socket: this library will be used to get the machine’s hostname which will be used when creating the Excel report
datetime: this library will be used to get the actual DateTime to create the Excel report
sys: System-specific parameters and functions
sqlite3: Built-in library which is used to interacting with SQLite, which will be the database engine used throughout the project
openpyxl: Python library for creating and working with Excel files that will be used as reports The os, sys, sqlite3, socket, and datetime libraries are included with the default out-of-the-box installation of Python. The openpyxl library can be installed with the pip install openpyxl or pip3 install openpyxl command, whichever applicable. You are free to use any other libraries or equivalent of these, as long as you accomplish the goals outlined in this project. When writing SQLite queries as part of the project, please use the following syntax (which is highly recommended to avoid SQL injections):

cursor.execute("SELECT * FROM table WHERE name=?", (name,)

Please note that if you are using Python 3.6 or higher, you might need to use pip3 instead of pip.

## Dataset

This project doesn’t include any specific dataset. The dataset required will be gathered from the file system of your machine. You will be able to choose which directories the application will monitor. Those folders will then be used to retrieve a list of files that will be used as the dataset.

-----
